/*
 * This file is part of gobject-sdk.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef INTERPEER_LIB_WYRD_HANDLE_H
#define INTERPEER_LIB_WYRD_HANDLE_H

#include <build-config.h>

#include <string>
#include <map>
#include <memory>
#include <atomic>

#include <wyrd/handle.h>
#include <vessel/author.h>
#include <liberate/logging.h>

namespace interpeer {

struct property_map_entry
{
  struct _WyrdHandle *  owner;

  std::string target_property{};
  std::string signal_name{};
  std::string wyrd_property_path{};

  GObject * target = nullptr;
  gulong    handler_id = 0;

  std::atomic<size_t> loop_count = 0;

  inline property_map_entry(struct _WyrdHandle * _owner,
      GObject * _target,
      std::string const & _target_property,
      std::string const & _signal_name,
      std::string const & _wyrd_property_path,
      GCallback callback)
    : owner{_owner}
    , target_property{_target_property}
    , signal_name{_signal_name}
    , wyrd_property_path{_wyrd_property_path}
    , target{g_object_ref(_target)}
  {
    if (callback) {
      handler_id = g_signal_connect(target, signal_name.c_str(), callback, this);
      LIBLOG_TRACE("Registered for '" << signal_name << "': " << handler_id);
    }
  }

  inline ~property_map_entry()
  {
    if (handler_id > 0) {
      g_signal_handler_disconnect(target, handler_id);
      LIBLOG_TRACE("Unregistered: " << handler_id);
    }
    g_object_unref(target);
  }


  inline bool try_protect()
  {
    auto val = loop_count.fetch_add(1);
    if (val > 0) {
      unprotect();
      return false;
    }
    return true;
  }

  inline void unprotect()
  {
    loop_count--;
  }
};


using property_map = std::map<
    gulong,
    std::unique_ptr<property_map_entry>
>;


using signal_map = std::map<
    GType,
    guint
>;


} // namespace interpeer

extern "C" {

struct _WyrdHandle
{
  GObject parent_instance;

  struct wyrd_handle * handle = nullptr;

  struct vessel_algorithm_choices algo_choices{};
  struct vessel_author * author = nullptr;

  interpeer::property_map properties{};
  interpeer::signal_map   signals{};
};

struct _WyrdHandleClass
{
  GObjectClass parent_class;
};



#define INTERPEER_WYRD_HANDLE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), INTERPEER_TYPE_WYRD_HANDLE, WyrdHandle))

} // extern "C"

#endif // guard
