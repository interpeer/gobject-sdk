/*
 * This file is part of gobject-sdk.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef INTERPEER_LIB_WYRD_ERROR_H
#define INTERPEER_LIB_WYRD_ERROR_H

#include <glib-object.h>

#include <wyrd/error.h>
#include <vessel/error.h>

#define INTERPEER_WYRD_ERROR(error, code) \
  g_set_error((error), \
      interpeer_wyrd_error_quark(), \
      (code), \
      "%s // %s", \
      wyrd_error_name(code), \
      wyrd_error_message(code));

#define INTERPEER_VESSEL_ERROR(error, code) \
  g_set_error((error), \
      interpeer_vessel_error_quark(), \
      (code), \
      "%s // %s", \
      vessel_error_name(code), \
      vessel_error_message(code));

#endif // guard
