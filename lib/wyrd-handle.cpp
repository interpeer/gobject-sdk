/*
 * This file is part of gobject-sdk.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <build-config.h>

#include <iostream> // FIXME
#include <cstring>

#include <wyrd/properties.h>
#include <liberate/logging.h>

#include <interpeer/gobject/wyrd-handle.h>
#include <interpeer/gobject/wyrd-api.h>

#include "wyrd-handle.h"
#include "wyrd-error.h"

namespace interpeer {

static constexpr auto WYRD_SIGNAL_PREFIX = "interpeer-wyrd-property-changed";

struct owned_memory
{
  char * buf = nullptr;
  size_t size = 0;

  inline owned_memory()
  {
  }

  inline ~owned_memory()
  {
    if (buf) {
      delete [] buf;
      buf = nullptr;
    }
    size = 0;
  }

  inline void allocate(size_t _size)
  {
    buf = new char[_size]{};
    size = _size;
  }
};


#define _MAKE_VALUE(type, gtype, setter_func) \
  { \
    if (sizeof(type) != value_size) { \
      LIBLOG_TRACE("Bad conversion; got value size " << value_size \
        << " but needed " << sizeof(type)); \
      return nullptr; \
    } \
    GValue * ret = static_cast<GValue *>(g_malloc(sizeof(GValue))); \
    *ret = G_VALUE_INIT; \
    g_value_init(ret, gtype); \
    setter_func(ret, *static_cast<type const *>(value)); \
    return ret; \
  }


inline GValue *
wyrd_to_gvalue(wyrd_property_type type, void const * value, size_t value_size)
{
  switch (type) {
    case WYRD_PT_SINT8:
      _MAKE_VALUE(int8_t, G_TYPE_CHAR, g_value_set_schar);
      break;

    case WYRD_PT_UINT8:
      _MAKE_VALUE(uint8_t, G_TYPE_UCHAR, g_value_set_uchar);
      break;

    case WYRD_PT_SINT16:
      _MAKE_VALUE(int16_t, G_TYPE_INT, g_value_set_int);
      break;

    case WYRD_PT_UINT16:
      _MAKE_VALUE(uint16_t, G_TYPE_UINT, g_value_set_uint);
      break;

    case WYRD_PT_SINT32:
      _MAKE_VALUE(int32_t, G_TYPE_LONG, g_value_set_long);
      break;

    case WYRD_PT_UINT32:
      _MAKE_VALUE(uint32_t, G_TYPE_ULONG, g_value_set_ulong);
      break;

    case WYRD_PT_SINT64:
      _MAKE_VALUE(int64_t, G_TYPE_INT64, g_value_set_int64);
      break;

    case WYRD_PT_UINT64:
      _MAKE_VALUE(uint64_t, G_TYPE_UINT64, g_value_set_uint64);
      break;

    case WYRD_PT_BOOL:
      _MAKE_VALUE(bool, G_TYPE_BOOLEAN, g_value_set_boolean);
      break;

    case WYRD_PT_FLOAT:
      _MAKE_VALUE(float, G_TYPE_FLOAT, g_value_set_float);
      break;

    case WYRD_PT_DOUBLE:
      _MAKE_VALUE(double, G_TYPE_DOUBLE, g_value_set_double);
      break;

    case WYRD_PT_UTF8_STRING:
      {
        // Need a temporary container because g_value_set_string() assumes
        // NUL-terminated strings.
        auto ptr = static_cast<char const *>(value);
        std::string tmp{ptr, ptr + value_size};
        GValue * ret = static_cast<GValue *>(g_malloc(sizeof(GValue)));
        *ret = G_VALUE_INIT;
        g_value_init(ret, G_TYPE_STRING);
        // g_value_set_string() *copies*
        g_value_set_string(ret, tmp.c_str());
        return ret;
      }

    default:
      LIBLOG_TRACE("Could not convert from wyrd type " << type << " to GValue.");
      break;
  }

  return nullptr;
}


#define _ALLOC_COPY_RET(type, getter_func, ret) \
  out.allocate(sizeof(type)); \
  *reinterpret_cast<type *>(out.buf) = getter_func(gvalue); \
  return ret;

inline wyrd_property_type
gvalue_to_wyrd(GValue * gvalue, owned_memory & out)
{
  // Unfortunately, we can't know which internal data structures GValue has -
  // that would be a brittle thing to rely on. So we allocate enough memory
  // for the value.
  auto type = G_VALUE_TYPE(gvalue);

  switch (type) {
    case G_TYPE_CHAR:
      _ALLOC_COPY_RET(int8_t, g_value_get_schar, WYRD_PT_SINT8);

    case G_TYPE_UCHAR:
      _ALLOC_COPY_RET(uint8_t, g_value_get_uchar, WYRD_PT_UINT8);

    case G_TYPE_INT:
      _ALLOC_COPY_RET(int16_t, g_value_get_int, WYRD_PT_SINT16);

    case G_TYPE_UINT:
      _ALLOC_COPY_RET(uint16_t, g_value_get_uint, WYRD_PT_UINT16);

    case G_TYPE_LONG:
      _ALLOC_COPY_RET(int32_t, g_value_get_long, WYRD_PT_SINT32);

    case G_TYPE_ULONG:
      _ALLOC_COPY_RET(uint32_t, g_value_get_ulong, WYRD_PT_UINT32);

    case G_TYPE_INT64:
      _ALLOC_COPY_RET(int64_t, g_value_get_int64, WYRD_PT_SINT64);

    case G_TYPE_UINT64:
      _ALLOC_COPY_RET(uint64_t, g_value_get_uint64, WYRD_PT_UINT64);

    case G_TYPE_BOOLEAN:
      _ALLOC_COPY_RET(bool, g_value_get_boolean, WYRD_PT_BOOL);

    case G_TYPE_FLOAT:
      _ALLOC_COPY_RET(float, g_value_get_float, WYRD_PT_FLOAT);

    case G_TYPE_DOUBLE:
      _ALLOC_COPY_RET(double, g_value_get_double, WYRD_PT_DOUBLE);

    case G_TYPE_STRING:
      {
        char const * val = g_value_get_string(gvalue);
        if (!val) {
          LIBLOG_TRACE("String value was NULL, ignoring.");
          return WYRD_PT_UNKNOWN;
        }
        size_t len = strlen(val);
        out.allocate(len);
        std::memcpy(out.buf, val, len);
        return WYRD_PT_UTF8_STRING;
      }

    // TODO more types
    default:
      LIBLOG_TRACE("Could not convert from GType " << type << " to wyrd.");
      break;
  }

//i#define 	G_TYPE_INVALID
//#define 	G_TYPE_NONE
//#define 	G_TYPE_INTERFACE
//#define 	G_TYPE_CHAR
//#define 	G_TYPE_UCHAR
//#define 	G_TYPE_BOOLEAN
//#define 	G_TYPE_INT
//#define 	G_TYPE_UINT
//#define 	G_TYPE_LONG
//#define 	G_TYPE_ULONG
//#define 	G_TYPE_INT64
//#define 	G_TYPE_UINT64
//#define 	G_TYPE_ENUM
//#define 	G_TYPE_FLAGS
//#define 	G_TYPE_FLOAT
//#define 	G_TYPE_DOUBLE
//#define 	G_TYPE_STRING
//#define 	G_TYPE_POINTER
//#define 	G_TYPE_BOXED
//#define 	G_TYPE_PARAM
//#define 	G_TYPE_OBJECT
//#define 	G_TYPE_GTYPE
//#define 	G_TYPE_VARIANT
//#define 	G_TYPE_CHECKSUM


#if 0
  switch (type)
    {
    case G_TYPE_BOOLEAN:
    case G_TYPE_CHAR:
    case G_TYPE_INT:
      rettype = &ffi_type_sint;
      *value = (gpointer)&(gvalue->data[0].v_int);
      break;
    case G_TYPE_ENUM:
      /* enums are stored in v_long even though they are integers, which makes
       * marshalling through libffi somewhat complicated.  They need to be
       * marshalled as signed ints, but we need to use a temporary int sized
       * value to pass to libffi otherwise it'll pull the wrong value on
       * BE machines with 32-bit integers when treating v_long as 32-bit int.
       */
      g_assert (enum_tmpval != NULL);
      rettype = &ffi_type_sint;
      *enum_tmpval = g_value_get_enum (gvalue);
      *value = enum_tmpval;
      *tmpval_used = TRUE;
      break;
    case G_TYPE_FLAGS:
      g_assert (enum_tmpval != NULL);
      rettype = &ffi_type_uint;
      *enum_tmpval = g_value_get_flags (gvalue);
      *value = enum_tmpval;
      *tmpval_used = TRUE;
      break;
    case G_TYPE_UCHAR:
    case G_TYPE_UINT:
      rettype = &ffi_type_uint;
      *value = (gpointer)&(gvalue->data[0].v_uint);
      break;
    case G_TYPE_STRING:
    case G_TYPE_OBJECT:
    case G_TYPE_BOXED:
    case G_TYPE_PARAM:
    case G_TYPE_POINTER:
    case G_TYPE_INTERFACE:
    case G_TYPE_VARIANT:
      rettype = &ffi_type_pointer;
      *value = (gpointer)&(gvalue->data[0].v_pointer);
      break;
    case G_TYPE_FLOAT:
      rettype = &ffi_type_float;
      *value = (gpointer)&(gvalue->data[0].v_float);
      break;
    case G_TYPE_DOUBLE:
      rettype = &ffi_type_double;
      *value = (gpointer)&(gvalue->data[0].v_double);
      break;
    case G_TYPE_LONG:
      rettype = &ffi_type_slong;
      *value = (gpointer)&(gvalue->data[0].v_long);
      break;
    case G_TYPE_ULONG:
      rettype = &ffi_type_ulong;
      *value = (gpointer)&(gvalue->data[0].v_ulong);
      break;
    case G_TYPE_INT64:
      rettype = &ffi_type_sint64;
      *value = (gpointer)&(gvalue->data[0].v_int64);
      break;
    case G_TYPE_UINT64:
      rettype = &ffi_type_uint64;
      *value = (gpointer)&(gvalue->data[0].v_uint64);
      break;
    default:
      rettype = &ffi_type_pointer;
      *value = NULL;
      g_critical ("value_to_ffi_type: Unsupported fundamental type: %s", g_type_name (type));
      break;
    }
  return rettype;
#endif
  return WYRD_PT_UNKNOWN;
}



inline void
property_signal_callback(struct wyrd_handle * handle [[maybe_unused]],
    char const * path [[maybe_unused]],
    wyrd_property_type type [[maybe_unused]],
    void const * value [[maybe_unused]],
    size_t value_size [[maybe_unused]],
    void * baton)
{
  if (!baton) {
    // Don't know what to do
    LIBLOG_TRACE("Empty baton; ignoring property callback.");
    return;
  }

  auto entry = static_cast<property_map_entry *>(baton);

  // Loop protection.
  if (!entry->try_protect()) {
    LIBLOG_TRACE("Loop protection, aborting.");
    return;
  }

  // XXX this may be nullptr, which may be the best we can do.
  auto val = wyrd_to_gvalue(type, value, value_size);
  LIBLOG_TRACE("Emitting signal: " << entry->signal_name << " with value "
      << static_cast<void *>(val));

  g_signal_emit_by_name(entry->target, entry->signal_name.c_str(), path, val);

  entry->unprotect();
}


inline void
notify_to_wyrd_callback(GObject * gobject, GParamSpec * pspec,
    gpointer baton)
{
  if (!baton) {
    // Don't know what to do
    LIBLOG_TRACE("Empty baton; ignoring notify callback.");
    return;
  }

  auto entry = static_cast<property_map_entry *>(baton);

  // Loop protection.
  if (!entry->try_protect()) {
    LIBLOG_TRACE("Loop protection, aborting.");
    return;
  }

  LIBLOG_TRACE("Notified that property '" << pspec->name << "' was changed.");

  // Grab current property value
  GValue value = G_VALUE_INIT;
  g_value_init(&value, pspec->value_type);
  g_object_get_property(gobject, pspec->name, &value);

  // Convert property value
  interpeer::owned_memory mem{};

  auto type = gvalue_to_wyrd(&value, mem);
  if (WYRD_PT_UNKNOWN == type) {
    LIBLOG_TRACE("Could not convert from GObject property, aborting.");
    entry->unprotect();
    return;
  }

  auto err = wyrd_set_property_typed(entry->owner->handle,
      entry->wyrd_property_path.c_str(), type,
      WYRD_MS_NAIVE_OVERRIDE,
      mem.buf, mem.size,
      1);
  if (WYRD_ERR_SUCCESS != err) {
    LIBLOG_TRACE("Failed to set wyrd property: " << wyrd_error_name(err));
    // fall through
  }

  entry->unprotect();
}



inline void
wyrd_to_notify_callback(struct wyrd_handle * handle [[maybe_unused]],
    char const * path [[maybe_unused]],
    wyrd_property_type type [[maybe_unused]],
    void const * value [[maybe_unused]],
    size_t value_size [[maybe_unused]],
    void * baton)
{
  if (!baton) {
    // Don't know what to do
    LIBLOG_TRACE("Empty baton; ignoring property callback.");
    return;
  }

  auto entry = static_cast<property_map_entry *>(baton);

  // Loop protection.
  if (!entry->try_protect()) {
    LIBLOG_TRACE("Loop protection, aborting.");
    return;
  }

  // XXX this may be nullptr, which may be the best we can do.
  auto val = wyrd_to_gvalue(type, value, value_size);
  LIBLOG_TRACE("Setting target property '" << entry->target_property << "'");

  g_object_set_property(entry->target, entry->target_property.c_str(), val);
  g_value_reset(val);
  g_free(val);

  entry->unprotect();
}



inline std::tuple<gulong, std::string>
property_signal_connect_internal(WyrdHandle * self,
    GObject * target,
    std::string const & target_property,  std::string const & signal_name,
    std::string const & property_path,
    GCallback gobj_callback, wyrd_property_callback wyrd_callback,
    GError ** error [[maybe_unused]])
{
  // Either use the signal name, or if none is given, create a signal for the
  // target's type.
  std::string signame;
  if (signal_name.empty()) {
    signame = WYRD_SIGNAL_PREFIX;
    signame += "::";
    signame += property_path;

    auto type = G_TYPE_FROM_INSTANCE(target);
    auto iter = self->signals.find(type);
    if (iter == self->signals.end()) {
      auto sig = g_signal_new(WYRD_SIGNAL_PREFIX,
          type,
          static_cast<GSignalFlags>(G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED),
          0, nullptr, nullptr, nullptr,
          G_TYPE_NONE,
          2, G_TYPE_STRING, G_TYPE_VALUE);
      self->signals[type] = sig;
    }
  }
  else {
    signame = signal_name;
  }

  LIBLOG_TRACE("Registering for " << signame);

  // Register the callback as signal handler for this signal.
  auto entry = std::make_unique<property_map_entry>(self,
      target, target_property, signame,
      property_path, gobj_callback);
  auto handler_id = entry->handler_id;

  // Also create a wyrd callback
  auto err = wyrd_add_property_callback(self->handle,
      property_path.c_str(), wyrd_callback, entry.get());
  if (WYRD_ERR_SUCCESS != err) {
    INTERPEER_WYRD_ERROR(error, err);
    return {0, {}};
  }

  // Remember and exit
  self->properties.insert({handler_id, std::move(entry)});
  return {handler_id, signame};
}

} // namespace interpeer

extern "C" {

G_DEFINE_FLAGS_TYPE(WyrdOpenFlags, interpeer_wyrd_open_flags,
    G_DEFINE_ENUM_VALUE(W_O_NONE, "open-none"),
    G_DEFINE_ENUM_VALUE(W_O_READ, "open-read"),
    G_DEFINE_ENUM_VALUE(W_O_WRITE, "open-write"),
    G_DEFINE_ENUM_VALUE(W_O_RW, "open-rw"),
    G_DEFINE_ENUM_VALUE(W_O_CREATE, "open-create"),
    G_DEFINE_ENUM_VALUE(W_O_SKIP_UNKNOWN, "open-skip-unknown")
)


G_DEFINE_TYPE(WyrdHandle, interpeer_wyrd_handle, G_TYPE_OBJECT)

static void
interpeer_wyrd_handle_dispose(GObject * gobject)
{
  WyrdHandle * handle = INTERPEER_WYRD_HANDLE(gobject);
  LIBLOG_TRACE("Disposing of WyrdHandle.");

  wyrd_close(&(handle->handle));
  vessel_author_free(&(handle->author));

  handle->signals.clear();
  handle->properties.clear();

  LIBLOG_TRACE("WyrdHandle disposed.");

  /* Always chain up to the parent class; there is no need to check if
   * the parent class implements the dispose() virtual function: it is
   * always guaranteed to do so
   */
  G_OBJECT_CLASS(interpeer_wyrd_handle_parent_class)->dispose(gobject);
}


static void
interpeer_wyrd_handle_finalize(GObject * gobject [[maybe_unused]])
{
  /* Always chain up to the parent class; as with dispose(), finalize()
   * is guaranteed to exist on the parent's class virtual function table
   */
  G_OBJECT_CLASS(interpeer_wyrd_handle_parent_class)->finalize(gobject);
  LIBLOG_TRACE("WyrdHandle finalized.");
}


static void
interpeer_wyrd_handle_class_init(WyrdHandleClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS(klass);

  object_class->dispose = interpeer_wyrd_handle_dispose;
  object_class->finalize = interpeer_wyrd_handle_finalize;
}


static void
interpeer_wyrd_handle_init(WyrdHandle * handle)
{
  handle->handle = nullptr;
  handle->author = nullptr;

  // A reasonable default
  handle->algo_choices = vessel_algorithm_choices_create(
    "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1"
  );

  handle->properties = interpeer::property_map{};
  handle->signals = interpeer::signal_map{};
}




gboolean
interpeer_wyrd_handle_set_author(WyrdHandle * self, gchar const * author,
    GError ** error)
{
  g_return_val_if_fail(INTERPEER_IS_WYRD_HANDLE(self), FALSE);
  g_return_val_if_fail((!error || !*error), FALSE);

  // The first thing to do is to create the author given the handle's current
  // algorithm choices.
  auto err = vessel_author_from_buffer(&(self->author), &(self->algo_choices),
      author, strlen(author));
  if (VESSEL_ERR_SUCCESS != err) {
    INTERPEER_VESSEL_ERROR(error, err);
    return FALSE;
  }

  auto werr = wyrd_set_resource_option(self->handle, WYRD_RO_AUTHOR,
      self->author);
  if (WYRD_ERR_SUCCESS != werr) {
    INTERPEER_WYRD_ERROR(error, werr);
    return FALSE;
  }

  // All good
  return true;
}



gboolean
interpeer_wyrd_handle_set_algorithm_choices(WyrdHandle * self, gchar const * choices,
    GError ** error)
{
  g_return_val_if_fail(INTERPEER_IS_WYRD_HANDLE(self), FALSE);
  g_return_val_if_fail((!error || !*error), FALSE);

  auto algo_choices = vessel_algorithm_choices_create(choices);
  if (!algo_choices.version_tag) {
    INTERPEER_VESSEL_ERROR(error, VESSEL_ERR_BAD_ALGORITHMS);
    return FALSE;
  }

  self->algo_choices = algo_choices;

  auto err = wyrd_set_resource_option(self->handle, WYRD_RO_ALGORITHMS,
      &(self->algo_choices));
  if (WYRD_ERR_SUCCESS != err) {
    INTERPEER_WYRD_ERROR(error, err);
    return FALSE;
  }

  return TRUE;
}


gboolean
interpeer_wyrd_handle_set_property(WyrdHandle * self, gchar const * path,
    GValue * value, GError ** error)
{
  g_return_val_if_fail(INTERPEER_IS_WYRD_HANDLE(self), FALSE);
  g_return_val_if_fail((!error || !*error), FALSE);
  g_return_val_if_fail(path, FALSE);
  g_return_val_if_fail(value, FALSE);

  interpeer::owned_memory mem{};

  auto type = gvalue_to_wyrd(value, mem);
  if (WYRD_PT_UNKNOWN == type) {
    INTERPEER_WYRD_ERROR(error, WYRD_ERR_INVALID_VALUE);
    return FALSE;
  }

  auto err = wyrd_set_property_typed(self->handle, path, type,
      WYRD_MS_NAIVE_OVERRIDE,
      mem.buf, mem.size,
      1);
  if (WYRD_ERR_SUCCESS != err) {
    INTERPEER_WYRD_ERROR(error, err);
    return FALSE;
  }

  return TRUE;
}


GValue *
interpeer_wyrd_handle_get_property(WyrdHandle * self, gchar const * path,
    GError ** error)
{
  g_return_val_if_fail(INTERPEER_IS_WYRD_HANDLE(self), NULL);
  g_return_val_if_fail((!error || !*error), NULL);
  g_return_val_if_fail(path, NULL);

  // Get wyrd type
  wyrd_property_type type = WYRD_PT_UNKNOWN;
  auto err = wyrd_get_property_type(self->handle, path, &type);
  if (WYRD_ERR_BAD_PROPERTY_TYPE == err) {
    // Property does not exist.
    return nullptr;
  }
  if (WYRD_ERR_SUCCESS != err) {
    INTERPEER_WYRD_ERROR(error, err);
    return nullptr;
  }
  if (WYRD_PT_UNKNOWN == type) {
    // Property type unknown; this is NULL
    return nullptr;
  }

  // Get value
  interpeer::owned_memory mem{};
  char dummy;

  err = wyrd_get_property(self->handle, path, &dummy, &mem.size);
  if (WYRD_ERR_SUCCESS == err) {
    // Convert from single byte.
    return interpeer::wyrd_to_gvalue(type, &dummy, sizeof(dummy));
  }
  else if (WYRD_ERR_OUT_OF_MEMORY != err) {
    INTERPEER_WYRD_ERROR(error, err);
    return nullptr;
  }

  // Allocate and fetch again.
  mem.allocate(mem.size);
  err = wyrd_get_property(self->handle, path, mem.buf, &mem.size);
  if (WYRD_ERR_SUCCESS != err) {
    INTERPEER_WYRD_ERROR(error, err);
    return nullptr;
  }

  return interpeer::wyrd_to_gvalue(type, mem.buf, mem.size);
}



gulong
interpeer_wyrd_handle_property_connect(WyrdHandle * self,
    GObject * target, gchar const * target_property, gchar const * wyrd_property_path,
    GError ** error)
{
  g_return_val_if_fail(INTERPEER_IS_WYRD_HANDLE(self), 0);
  g_return_val_if_fail((!error || !*error), 0);
  g_return_val_if_fail(target, 0);
  g_return_val_if_fail(target_property, 0);
  g_return_val_if_fail(wyrd_property_path, 0);

  std::string signal_name{"notify::"};
  signal_name += target_property;
  auto [handler_id, signame] = interpeer::property_signal_connect_internal(self,
      target,
      target_property, signal_name,
      wyrd_property_path,
      G_CALLBACK(interpeer::notify_to_wyrd_callback),
      interpeer::wyrd_to_notify_callback,
      error);
  return handler_id;
}


gchar const *
interpeer_wyrd_handle_setup_property_signal(WyrdHandle * self,
    GObject * target, gchar const * property_path,
    GError ** error)
{
  g_return_val_if_fail(INTERPEER_IS_WYRD_HANDLE(self), 0);
  g_return_val_if_fail((!error || !*error), 0);
  g_return_val_if_fail(target, 0);
  g_return_val_if_fail(property_path, 0);

  auto [handler_id, signame] = interpeer::property_signal_connect_internal(self,
      target,
      {}, {},
      property_path,
      nullptr,
      interpeer::property_signal_callback,
      error);

  return g_strdup(signame.c_str());
}

} // extern "C"
