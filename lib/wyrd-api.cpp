/*
 * This file is part of gobject-sdk.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <build-config.h>

#include <interpeer/gobject/wyrd-api.h>

#include <wyrd/api.h>

#include "wyrd-handle.h"
#include "wyrd-error.h"

#define INTERPEER_WYRD_API(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), INTERPEER_TYPE_WYRD_API, WyrdApi))

extern "C" {

GQuark interpeer_wyrd_error_quark(void)
{
  static GQuark quark = 0;
  if (!quark) {
    quark = g_quark_from_static_string("interpeer/wyrd error");
  }
  return quark;
}


GQuark interpeer_vessel_error_quark(void)
{
  static GQuark quark = 0;
  if (!quark) {
    quark = g_quark_from_static_string("interpeer/vessel error");
  }
  return quark;
}


struct _WyrdApi
{
  GObject parent_instance;

  struct wyrd_api * api = nullptr;
};


struct _WyrdApiClass
{
  GObjectClass parent_class;
};


G_DEFINE_TYPE(WyrdApi, interpeer_wyrd_api, G_TYPE_OBJECT)

static void
interpeer_wyrd_api_dispose(GObject * gobject)
{
  auto api = INTERPEER_WYRD_API(gobject);

  wyrd_destroy(&(api->api));

  /* Always chain up to the parent class; there is no need to check if
   * the parent class implements the dispose() virtual function: it is
   * always guaranteed to do so
   */
  G_OBJECT_CLASS(interpeer_wyrd_api_parent_class)->dispose(gobject);
}


static void
interpeer_wyrd_api_finalize(GObject * gobject [[maybe_unused]])
{
  /* Always chain up to the parent class; as with dispose(), finalize()
   * is guaranteed to exist on the parent's class virtual function table
   */
  G_OBJECT_CLASS(interpeer_wyrd_api_parent_class)->finalize(gobject);
}


static void
interpeer_wyrd_api_class_init(WyrdApiClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS(klass);

  object_class->dispose = interpeer_wyrd_api_dispose;
  object_class->finalize = interpeer_wyrd_api_finalize;
}


static void
interpeer_wyrd_api_init(WyrdApi * api)
{
  api->api = nullptr;
  wyrd_create(&(api->api));
}


WyrdHandle * interpeer_wyrd_api_open_file(WyrdApi * self, char const * filename, WyrdOpenFlags flags, GError ** error)
{
  g_return_val_if_fail(INTERPEER_IS_WYRD_API(self), nullptr);
  g_return_val_if_fail((!error || !*error), nullptr);

  wyrd_handle * handle = nullptr;
  auto err = wyrd_open_file(self->api, &handle, filename, flags);
  if (WYRD_ERR_SUCCESS != err) {
    INTERPEER_WYRD_ERROR(error, err);
    return nullptr;
  }

  WyrdHandle * ret = (WyrdHandle *) g_object_new(INTERPEER_TYPE_WYRD_HANDLE, NULL);
  ret->handle = handle;
  return ret;
}


WyrdHandle * interpeer_wyrd_api_open_resource(WyrdApi * self, char const * identifier, WyrdOpenFlags flags, GError ** error)
{
  g_return_val_if_fail(INTERPEER_IS_WYRD_API(self), nullptr);
  g_return_val_if_fail((!error || !*error), nullptr);

  wyrd_handle * handle = nullptr;
  auto err = wyrd_open_resource(self->api, &handle, identifier, flags);
  if (WYRD_ERR_SUCCESS != err) {
    INTERPEER_WYRD_ERROR(error, err);
    return nullptr;
  }

  WyrdHandle * ret = (WyrdHandle *) g_object_new(INTERPEER_TYPE_WYRD_HANDLE, NULL);
  ret->handle = handle;
  return ret;
}



} // extern "C"

