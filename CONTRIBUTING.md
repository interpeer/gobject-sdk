# Contributing

We love merge requests from everyone. By participating in this project, you
agree to abide by the following documents:

* [Code of Conduct](./CODE_OF_CONDUCT.md)
* [License](./LICENSE)
* [Developer Certificate of Origin](./DCO.txt)
  as applied to the license above.
* Additionally, you assign all copyright to Interpeer gUG (haftungsbeschränkt).
  See below.

## Copyright

Let's digest that.

*Interpeer gUG (haftungsbeschränkt)* is a non-profit dedicated to furthering research
and development into human centric, next generation internet technologies,
incorporated in Germany on June 1st 2022. This is, in a nutshell, the entity's
chartered purpose.

It's public interest status (indicated by the lower case "g") depends on the
entity fulfilling said purpose *and making results available to the public*.
In terms of development, making results available to the public is equivalent
to providing public code.

Being a public interest *company* means it can also engage in *limited*
commercial activities, such as selling commercial licenses to the software,
which can help fund its activities -- but it cannot generate a profit for
its shareholders, and instead has to re-invest such earnings into its mission.

In the meantime, all of your contributions are *still* free, because the
license file includes the GPL.

That's probably the best vehicle for ensuring the continued maintenance of
this software.

## Fork Me!

Fork, then clone the repo:

    git clone git@codeberg.org:your-username/gobject-sdk.git

## Setup

The two main requiremenst for building are [meson](https://mesonbuild.com)
and [ninja](https://ninja-build.org/). The latter is available on most
operating systems, and ships with Visual Studio on Windows.

Meson is a [Python](https://www.python.org/) based build system. We strive
to select all development tools as Python packages, to minimize dependencies.
To make installation of these tools simplest, we provide a `Pipfile`.

* Install [Python](https://www.python.org/). Make sure you install the
  `pip` package manager as well; this is the default. Make sure it's a
  version of Python 3, because 2 is no longer maintained.
  * On Windows, you might want to add both the directory holding the
    the `python.exe` file as well as it's sibling, the `Scripts` directory
    to your `%PATH%`.
  * On \*NIX, it tends to be the case that when `python` is in your path,
    so are the scripts it may install.
* Install [pipenv](https://pipenv.pypa.io/en/latest/)
  ```bash
  $ pip install pipenv [--user]
  ```
* Install development tools
  ```bash
  $ pipenv install
  ```
  You can now access all those development tools when you enter the `pipenv`
  shell:
  ```bash
  $ pipenv shell
  ```
* On Windows, make sure to run `vcvarsall.bat` with the Windows SDK settings
  you want. Meson will find the C++ compiler and ninja from the environment
  variables set by the script.

## Building

* Create a build directory. Meson won't build directly in the source
  directory, and that's usually a good thing. Let's call it `build`.
* Run meson (in `pipenv shell`):
  ```bash
  $ cd build
  $ meson /path/to/sources
  ```
* Run ninja:
  ```bash
  $ cd build
  $ ninja
  ```

## Testing

The meson build system produces a `test` task for ninja, which runs all tests
after building them. Unfortunately for us, that suppresses output of the test
program, and expects each test case to be its own executable.

That may make sense for your project. For us, though, we're using
[googletest](https://github.com/google/googletest) as a unit test runner, which
means the test case status gets lost here. Just run the executables manually.

## Static Code Analysis

The `Pipfile` attempts to install:

* The [semgrep](https://semgrep.dev) CLI.
* The [flawfinder](https://dwheeler.com/flawfinder/) tool.

Furthermore, the `meson.build` file contains extra targets for tools that
may exist on the system:

* `cppcheck` for running [cppcheck](http://cppcheck.net/)
* `oclint` for running [OCLint](https://oclint.org/)

Some of these are run as part of the CI pipeline.

## Code Coverage

At least if you use GCC, you can generate code coverage results for unit tests
from meson as follows:

* Run `meson .. -Db_coverage=true`, then build as usual
* Run unit tests
* Run `ninja coverage` (if using ninja)

Code coverage reports will be available in the `meson-logs/coveragereport`
subdirectory.

## Pull Requests

You can create merge requests however you like. However, review will likely
require you to add a changelong snippet. This project uses [towncrier](https://github.com/twisted/towncrier)
for building changelogs. In practice, that means you should add a
changelog snippet in `changelog.d/<issue>.<reason>`, where the first part is
the issue number you're addressing, and the second a keyword such as `feature`
or `bugfix` (see towncrier documentation for details).

You can run towncrier to see the generated changelog yourself, but generating a
changelog from snippets is *not* supposed to be part of the MR. The maintainer
collects snippets from MRs, and creates the merged changelog during release.

```bash
$ towncrier --draft --version v0.1.2
```

**Note**: the version above is unimportant for generating the draft, but
important to the tool. Just add anything.
