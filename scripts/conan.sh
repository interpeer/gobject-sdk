#!/bin/sh

set -ex

SOURCE_DIR=$(dirname $(dirname $(realpath "$0")))
CODEMETA="${SOURCE_DIR}/codemeta.json"
PACKAGE=$(cat "${CODEMETA}" | jq .name | sed 's/"//g')

PROFILES="default
  android-arm64-v8a.conan
  android-armeabi-v7a.conan
  android-x86.conan
  android-x86_64.conan"

for profile in ${PROFILES} ; do
  conan create --profile:host=${profile} \
               --profile:build=default \
               --test-folder=None \
               -s build_type=Release \
               --build=missing \
               .
done

if [ "$1" = '--upload' ] ; then
  conan upload --remote=codeberg "${PACKAGE}"'/*' --all --confirm
fi
