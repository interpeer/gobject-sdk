##############################################################################
# Tests

# Tests in a subproject are complicicated. You need all the compile and
# link flags from the enclosing project, etc.
if not meson.is_subproject() and get_option('build_extras')
  gtest = subproject('gtest')

  test_env = lsan_env + asan_env

  # See https://github.com/google/googletest/issues/813
  test_args = []
  if host_machine.system() == 'cygwin'
    test_args += ['-D_POSIX_C_SOURCE=200809L']
  endif

  # Google test issues this warning; disable it in *test* code only.
  if compiler_id == 'msvc'
    test_args = [
      '/wd4389',
    ]
  endif

  # We're building two tests:
  # - public_tests include *only* public headers
  # - private_tests include private headers

  testsuite_src = [
    'properties.cpp',
    'runner.cpp',
  ]


  testsuite_tests = executable('testsuite', testsuite_src,
      dependencies: [
        main_build_dir, 
        dep_internal,
        gtest.get_variable('gtest_dep'),
      ],
      cpp_args: test_args,
  )
  test('testsuite', testsuite_tests, env: test_env)

  # C compatibility tests are also public tests of sorts
  c_compiler = meson.get_compiler('c')
  c_compatibility = executable('c_compatibility', 'c_compatibility.c',
    dependencies: [
      dep_internal,
    ],
    link_language: 'c',
  )
  test('c_compatibility', c_compatibility, env: test_env)

  # Python tests - this automatically tests GIR based language bindings.
  if host_type == 'posix' and get_option('b_sanitize') == 'none'
    glib_path = 'glib-@0@'.format(glib_dep.version())
    py_env = [
      'LD_LIBRARY_PATH=@0@:@1@:@2@'.format(
        meson.project_build_root(),
        meson.project_build_root() / 'subprojects' / glib_path / 'glib',
        meson.project_build_root() / 'subprojects' / glib_path / 'gobject',
      ),
      'GI_TYPELIB_PATH=@0@'.format(meson.project_build_root()),
    ]

    py = find_program('pytest')
    test('pytest', py,
      args: ['--verbose', '--color=yes', meson.current_source_dir() / 'python_binding'],
      env: py_env + test_env,
    )
  endif
endif
