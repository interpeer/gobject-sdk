/**
 * This file is part of gobject-sdk.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <iostream>

#include <gtest/gtest.h>

#include "env.h"

#include <interpeer/gobject.h>

TestEnvironment * test_env = nullptr;

int main(int argc, char **argv)
{
  std::cout << interpeer_copyright_string() << std::endl;

  try {
    test_env = new TestEnvironment();
  } catch (std::exception const & ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
  }


  // Ownership passes to gtest here.
  ::testing::AddGlobalTestEnvironment(test_env);
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
