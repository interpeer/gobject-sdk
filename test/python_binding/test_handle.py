# -*- coding: utf-8 -*-
"""TODO"""

__author__ = 'Jens Finkhaeuser'
__copyright__ = 'Copyright (c) 2023 Interpeer gUG (haftungsbeschraenkt)'
__license__ = 'GPLv3'
__all__ = ()

import pytest

from . import interpeer, sandbox

def test_create_file(interpeer, tmpdir):
    with sandbox.sandbox(tmpdir):
        api = interpeer.WyrdApi()
        flags = interpeer.WyrdOpenFlags.OPEN_RW | interpeer.WyrdOpenFlags.OPEN_CREATE

        # Create a file handle
        handle = api.open_file("testfile", flags)
        assert handle is not None


def test_create_resource_basic(interpeer, tmpdir):
    with sandbox.sandbox(tmpdir):
        api = interpeer.WyrdApi()
        flags = interpeer.WyrdOpenFlags.OPEN_RW | interpeer.WyrdOpenFlags.OPEN_CREATE

        # Create resource handle
        handle = api.open_resource("testresource", flags)
        assert handle is not None


def test_create_resource_bad_algorithms(interpeer, tmpdir):
    with sandbox.sandbox(tmpdir):
        api = interpeer.WyrdApi()
        flags = interpeer.WyrdOpenFlags.OPEN_RW | interpeer.WyrdOpenFlags.OPEN_CREATE

        # Create resource handle
        handle = api.open_resource("testresource", flags)
        assert handle is not None

        # Bad algo choices
        import gi
        with pytest.raises(gi.repository.GLib.GError) as ex:
            tmp = handle.set_algorithm_choices('')
            assert tmp == False
        assert 'VESSEL_ERR_BAD_ALGORITHMS' in str(ex.value)


def test_create_resource_bad_author(interpeer, tmpdir):
    with sandbox.sandbox(tmpdir):
        api = interpeer.WyrdApi()
        flags = interpeer.WyrdOpenFlags.OPEN_RW | interpeer.WyrdOpenFlags.OPEN_CREATE

        # Create resource handle
        handle = api.open_resource("testresource", flags)
        assert handle is not None

        # Bad author
        import gi
        with pytest.raises(gi.repository.GLib.GError) as ex:
            tmp = handle.set_author('')
            assert tmp == False
        assert 'VESSEL_ERR_KEY_SERIALIZATION' in str(ex.value)


def test_create_resource_success(interpeer, tmpdir):
    with sandbox.sandbox(tmpdir):
        api = interpeer.WyrdApi()
        flags = interpeer.WyrdOpenFlags.OPEN_RW | interpeer.WyrdOpenFlags.OPEN_CREATE

        # Create resource handle
        handle = api.open_resource("testresource", flags)
        assert handle is not None

        # Set algo choices
        tmp = handle.set_algorithm_choices('sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=0')
        assert tmp == True

        # Set author
        tmp = handle.set_author('''-----BEGIN PRIVATE KEY-----
MC4CAQAwBQYDK2VwBCIEIE6V3BprgqVmv18GD75hBkxAUMLZM8KTy7JntEA1abN6
-----END PRIVATE KEY-----''')
        assert tmp == True
