# -*- coding: utf-8 -*-
"""TODO"""

__author__ = 'Jens Finkhaeuser'
__copyright__ = 'Copyright (c) 2023 Interpeer gUG (haftungsbeschraenkt)'
__license__ = 'GPLv3'
__all__ = ()

import pytest

from . import interpeer, sandbox, create_resource

from gi.repository import GObject

class ObjectWithProperty(GObject.Object):
    @GObject.Property(type=int)
    def prop_gint(self):
        """Read-write integer property."""
        if not hasattr(self, 'value'):
            return None
        return self.value

    @prop_gint.setter
    def prop_gint(self, value):
        self.value = value


def test_set_property(interpeer, tmpdir):
    with sandbox.sandbox(tmpdir):
        handle = create_resource(interpeer)

        ret = handle.set_property(".foo.bar", 42)
        assert ret


def test_get_property(interpeer, tmpdir):
    with sandbox.sandbox(tmpdir):
        handle = create_resource(interpeer)

        val = handle.get_property(".foo.bar")
        assert val is None

        ret = handle.set_property(".foo.bar", 42)
        assert ret

        val = handle.get_property("foo.bar")
        assert val == 42


def test_property_signal(interpeer, tmpdir):
    with sandbox.sandbox(tmpdir):
        handle = create_resource(interpeer)

        # Register a property callback
        signal = handle.setup_property_signal(handle, ".foo.bar")

        found = False
        for sig in GObject.signal_list_names(handle):
            if sig in signal:
                found = True
                break
        assert found

        # Connect the callback to the signal
        received = None
        def func(h, p, v, *args, **kw):
            nonlocal received
            received = v
        handle.connect(signal, func)

        # If we now set the property, the callback should be called.
        ret = handle.set_property(".foo.bar", 42)
        assert ret
        assert received == 42


def test_property_signal_other_object(interpeer, tmpdir):
    with sandbox.sandbox(tmpdir):
        handle = create_resource(interpeer)

        obj = ObjectWithProperty()

        # Register a property callback
        signal = handle.setup_property_signal(obj, ".foo.bar")

        # Connect the callback to the signal
        received = None
        def func(h, p, v, *args, **kw):
            nonlocal received
            received = v
        obj.connect(signal, func)

        # If we now set the property, the callback should be called.
        ret = handle.set_property(".foo.bar", 42)
        assert ret
        assert received == 42


def test_property_connect(interpeer, tmpdir):
    with sandbox.sandbox(tmpdir):
        handle = create_resource(interpeer)

        obj = ObjectWithProperty()
        obj.set_property("prop-gint", 42)
        assert 42 == obj.get_property("prop-gint")

        # Connect wyrd property with gobject property
        res = handle.property_connect(obj, "prop-gint", ".foo.bar")

        # Set Gobject property - this should result in the wyrd property
        # being set.
        obj.set_property("prop-gint", 321)
        val = handle.get_property(".foo.bar")
        assert 321 == val

        # Set wyrd property - this should update the gobject property
        ret = handle.set_property(".foo.bar", 123)
        assert ret
        assert 123 == obj.get_property("prop-gint")
