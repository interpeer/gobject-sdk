# -*- coding: utf-8 -*-
"""TODO"""

__author__ = 'Jens Finkhaeuser'
__copyright__ = 'Copyright (c) 2023 Interpeer gUG (haftungsbeschraenkt)'
__license__ = 'GPLv3'
__all__ = ()

import pytest

@pytest.fixture
def interpeer():
    import gi

    gi.require_version("interpeer", "1.0")
    from gi.repository import interpeer

    return interpeer

def create_resource(interpeer):
    api = interpeer.WyrdApi()
    flags = interpeer.WyrdOpenFlags.OPEN_RW | interpeer.WyrdOpenFlags.OPEN_CREATE

    # Create resource handle
    handle = api.open_resource("testresource", flags)
    assert handle is not None

    # Set algo choices
    tmp = handle.set_algorithm_choices('sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1')
    assert tmp == True

    # Set author
    tmp = handle.set_author('''-----BEGIN PRIVATE KEY-----
MC4CAQAwBQYDK2VwBCIEIE6V3BprgqVmv18GD75hBkxAUMLZM8KTy7JntEA1abN6
-----END PRIVATE KEY-----''')
    assert tmp == True

    return handle
