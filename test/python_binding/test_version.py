# -*- coding: utf-8 -*-
"""TODO"""

__author__ = 'Jens Finkhaeuser'
__copyright__ = 'Copyright (c) 2023 Interpeer gUG (haftungsbeschraenkt)'
__license__ = 'GPLv3'
__all__ = ()

from . import interpeer

def test_can_access_version_string(interpeer):
    tmp = interpeer.license_string()
    assert 'GNU' in tmp

    tmp = interpeer.copyright_string()
    assert 'GNU' in tmp

    # To make the test reliable, don't expect any particular version
    # components - but they must add up to something positive.
    tmp = interpeer.VersionData()
    s = tmp.major + tmp.minor + tmp.patch
    assert s > 0
