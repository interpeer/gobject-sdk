/**
 * This file is part of gobject-sdk.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#include <interpeer/gobject.h>

#define TEST_FILE "testfile"

int main(int argc, char **argv)
{
  WyrdApi * api = (WyrdApi *) g_object_new(INTERPEER_TYPE_WYRD_API, NULL);
  GError * err = NULL;

  WyrdHandle * handle = (WyrdHandle *) interpeer_wyrd_api_open_file(api,
      TEST_FILE, W_O_RW | W_O_CREATE, &err);

  // This is enough to know that the C compiler didn't complain.

cleanup:
  g_object_unref(G_OBJECT(handle));
  g_object_unref(G_OBJECT(api));
  unlink(TEST_FILE);
}
