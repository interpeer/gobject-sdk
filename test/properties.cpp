/**
 * This file is part of gobject-sdk.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <cstring>

#include <interpeer/gobject.h>

#include <liberate/fs/tmp.h>

namespace {



G_BEGIN_DECLS

typedef struct TestObject
{
  GObject parent_instance;

  char *    string;

  int8_t    i8;
  uint8_t   u8;
  int16_t   i16;
  uint16_t  u16;
  int32_t   i32;
  uint32_t  u32;
  int64_t   i64;
  uint64_t  u64;

  bool      b;

  float     f;
  double    d;
} TestObject;

typedef struct TestObjectClass
{
  GObjectClass parent_class;
} TestObjectClass;


GType test_object_get_type(void) G_GNUC_CONST;

#define TEST_OBJECT_TYPE_HANDLE (test_object_get_type())
#define TEST_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), TEST_OBJECT_TYPE_HANDLE, TestObject))
#define TEST_OBJECT_IS_HANDLE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), TEST_OBJECT_TYPE_HANDLE))
#define TEST_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), TEST_OBJECT_TYPE_HANDLE, TestObjectClass))
#define TEST_OBJECT_IS_HANDLE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), TEST_OBJECT_TYPE_HANDLE))
#define TEST_OBJECT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), TEST_OBJECT_TYPE_HANDLE, TestObjectClass))

G_END_DECLS

G_DEFINE_TYPE(TestObject, test_object, G_TYPE_OBJECT)

static void
test_object_init(TestObject *self)
{
  self->string = nullptr;
  self->i8 = 0;
  self->u8 = 0;
  self->i16 = 0;
  self->u16 = 0;
  self->i32 = 0;
  self->u32 = 0;
  self->i64 = 0;
  self->u64 = 0;
  self->b = false;
  self->f = 0;
  self->d = 0;
}


static void
test_object_dispose(GObject * gobj)
{
  G_OBJECT_CLASS(test_object_parent_class)->dispose(gobj);
}


static void
test_object_finalize(GObject * gobj)
{
  auto self = TEST_OBJECT(gobj);

  if (self->string) {
    g_free(self->string);
    self->string = nullptr;
  }

  G_OBJECT_CLASS(test_object_parent_class)->finalize(gobj);
}



typedef enum
{
  PROP_STRING = 1,
  PROP_I8,
  PROP_U8,
  PROP_I16,
  PROP_U16,
  PROP_I32,
  PROP_U32,
  PROP_I64,
  PROP_U64,
  PROP_B,
  PROP_F,
  PROP_D,

  N_PROPERTIES
} TestObjectProperty;


static GParamSpec * obj_properties[N_PROPERTIES] = { NULL, };


static void
test_object_set_property(GObject * object, guint property_id,
    GValue const * value, GParamSpec * pspec)
{
  TestObject * self = TEST_OBJECT(object);

  switch ((TestObjectProperty) property_id) {
    case PROP_I8:
      self->i8 = g_value_get_schar(value);
      break;

    case PROP_U8:
      self->u8 = g_value_get_uchar(value);
      break;

    case PROP_I16:
      self->i16 = g_value_get_int(value);
      break;

    case PROP_U16:
      self->u16 = g_value_get_uint(value);
      break;

    case PROP_I32:
      self->i32 = g_value_get_long(value);
      break;

    case PROP_U32:
      self->u32 = g_value_get_ulong(value);
      break;

    case PROP_I64:
      self->i64 = g_value_get_int64(value);
      break;

    case PROP_U64:
      self->u64 = g_value_get_uint64(value);
      break;

    case PROP_B:
      self->b = g_value_get_boolean(value);
      break;

    case PROP_F:
      self->f = g_value_get_float(value);
      break;

    case PROP_D:
      self->d = g_value_get_double(value);
      break;

    case PROP_STRING:
      g_free(self->string);
      self->string = g_value_dup_string(value);
      break;

    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
      break;
  }
}


static void
test_object_get_property(GObject * object, guint property_id, GValue * value,
    GParamSpec * pspec)
{
  TestObject * self = TEST_OBJECT(object);

  switch ((TestObjectProperty) property_id) {
    case PROP_I8:
      g_value_set_schar(value, self->i8);
      break;

    case PROP_U8:
      g_value_set_uchar(value, self->u8);
      break;

    case PROP_I16:
      g_value_set_int(value, self->i16);
      break;

    case PROP_U16:
      g_value_set_uint(value, self->u16);
      break;

    case PROP_I32:
      g_value_set_long(value, self->i32);
      break;

    case PROP_U32:
      g_value_set_ulong(value, self->u32);
      break;

    case PROP_I64:
      g_value_set_int64(value, self->i64);
      break;

    case PROP_U64:
      g_value_set_uint64(value, self->u64);
      break;

    case PROP_B:
      g_value_set_boolean(value, self->b);
      break;

    case PROP_F:
      g_value_set_float(value, self->f);
      break;

    case PROP_D:
      g_value_set_double(value, self->d);
      break;

    case PROP_STRING:
      g_value_set_string(value, self->string);
      break;

    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
      break;
  }
}


static void
test_object_class_init(TestObjectClass * klass)
{
  GObjectClass * object_class = G_OBJECT_CLASS(klass);

  object_class->set_property = test_object_set_property;
  object_class->get_property = test_object_get_property;

  object_class->dispose = test_object_dispose;
  object_class->finalize = test_object_finalize;

  obj_properties[PROP_STRING] =
    g_param_spec_string("string", "string", "string",
        nullptr,
        G_PARAM_READWRITE);

  obj_properties[PROP_I8] =
    g_param_spec_char("i8", "i8", "i8",
        0, 100, 0,
        G_PARAM_READWRITE);

  obj_properties[PROP_U8] =
    g_param_spec_uchar("u8", "u8", "u8",
        0, 100, 0,
        G_PARAM_READWRITE);

  obj_properties[PROP_I16] =
    g_param_spec_int("i16", "i16", "i16",
        0, 100, 0,
        G_PARAM_READWRITE);

  obj_properties[PROP_U16] =
    g_param_spec_uint("u16", "u16", "u16",
        0, 100, 0,
        G_PARAM_READWRITE);

  obj_properties[PROP_I32] =
    g_param_spec_long("i32", "i32", "i32",
        0, 100, 0,
        G_PARAM_READWRITE);

  obj_properties[PROP_U32] =
    g_param_spec_ulong("u32", "u32", "u32",
        0, 100, 0,
        G_PARAM_READWRITE);

  obj_properties[PROP_I64] =
    g_param_spec_int64("i64", "i64", "i64",
        0, 100, 0,
        G_PARAM_READWRITE);

  obj_properties[PROP_U64] =
    g_param_spec_uint64("u64", "u64", "u64",
        0, 100, 0,
        G_PARAM_READWRITE);

  obj_properties[PROP_B] =
    g_param_spec_boolean("b", "b", "b",
        FALSE,
        G_PARAM_READWRITE);

  obj_properties[PROP_F] =
    g_param_spec_float("f", "f", "f",
        0, 100, 0,
        G_PARAM_READWRITE);

  obj_properties[PROP_D] =
    g_param_spec_double("d", "d", "d",
        0, 100, 0,
        G_PARAM_READWRITE);

  g_object_class_install_properties(object_class,
      N_PROPERTIES, obj_properties);
}





static const char ALGORITHMS[] = "sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1";
static const char AUTHOR_KEY[] = "-----BEGIN PRIVATE KEY-----\n"
  "MC4CAQAwBQYDK2VwBCIEIE6V3BprgqVmv18GD75hBkxAUMLZM8KTy7JntEA1abN6\n"
  "-----END PRIVATE KEY-----";

struct test_context
{
  WyrdApi * api = nullptr;
  WyrdHandle * handle = nullptr;
  TestObject * obj = nullptr;
  std::string identifier{};

  inline test_context()
    : identifier{liberate::fs::temp_name("gobject-properties")}
  {
    // Create API
    api = reinterpret_cast<WyrdApi *>(g_object_new(INTERPEER_TYPE_WYRD_API, NULL));

    // Create resource
    GError * err = nullptr;
    handle = reinterpret_cast<WyrdHandle *>(interpeer_wyrd_api_open_resource(
          api, identifier.c_str(),
          static_cast<WyrdOpenFlags>(W_O_RW | W_O_CREATE), &err));
    EXPECT_EQ(err, nullptr);
    EXPECT_NE(handle, nullptr);

    // Set algorithms
    auto ret = interpeer_wyrd_handle_set_algorithm_choices(handle, ALGORITHMS,
        &err);
    EXPECT_TRUE(ret);
    EXPECT_EQ(err, nullptr);

    // Set author
    ret = interpeer_wyrd_handle_set_author(handle, AUTHOR_KEY, &err);
    EXPECT_TRUE(ret);
    EXPECT_EQ(err, nullptr);

    // Test object
    obj = reinterpret_cast<TestObject *>(g_object_new(TEST_OBJECT_TYPE_HANDLE, NULL));
  }

  inline ~test_context()
  {
    g_object_unref(G_OBJECT(obj));
    g_object_unref(G_OBJECT(handle));
    g_object_unref(G_OBJECT(api));
    unlink(identifier.c_str());
  }
};


static size_t called = 0;

inline void
test_callback(GObject *, GParamSpec *, gpointer)
{
  ++called;
}



template <typename T>
bool is_eq(T const & first, T const & second)
{
  return (first == second);
}


template <>
bool is_eq<char const *>(char const * const & first, char const * const & second)
{
  return 0 == std::strcmp(first, second);
}


template <typename T, typename getT, typename setT>
inline void test_propagate_to_wyrd(char const * name, T const & value,
    GType gtype,
    getT getter, setT setter)
{
  test_context ctx;

  // Set up connection
  GError * err = nullptr;
  auto handler_id = interpeer_wyrd_handle_property_connect(ctx.handle,
      G_OBJECT(ctx.obj), name, ".foo.bar", &err);
  ASSERT_GT(handler_id, 0);
  ASSERT_EQ(err, nullptr);

  // Set gobject property
  GValue val = G_VALUE_INIT;
  g_value_init(&val, gtype);
  setter(&val, value);
  g_object_set_property(G_OBJECT(ctx.obj), name, &val);
  g_value_unset(&val);

  // Get value from wyrd
  auto prop = interpeer_wyrd_handle_get_property(ctx.handle,
      ".foo.bar", &err);
  ASSERT_NE(prop, nullptr);
  ASSERT_EQ(err, nullptr);
  T res = getter(prop);

  ASSERT_TRUE(is_eq(T{value}, res));
  g_value_unset(prop);
  g_free(prop);
}



template <typename T, typename getT, typename setT>
inline void test_propagate_to_gobject(char const * name, T const & value,
    GType gtype, getT getter, setT setter)
{
  test_context ctx;

  // Set up connection
  GError * err = nullptr;
  auto handler_id = interpeer_wyrd_handle_property_connect(ctx.handle,
      G_OBJECT(ctx.obj), name, ".foo.bar", &err);
  ASSERT_GT(handler_id, 0);
  ASSERT_EQ(err, nullptr);

  // Set wyrd property
  GValue val = G_VALUE_INIT;
  g_value_init(&val, gtype);
  setter(&val, value);
  auto ret = interpeer_wyrd_handle_set_property(ctx.handle,
      ".foo.bar", &val, &err);
  ASSERT_TRUE(ret);
  ASSERT_EQ(err, nullptr);

  // Get value from object property
  g_value_reset(&val);
  g_object_get_property(G_OBJECT(ctx.obj), name, &val);
  T res = getter(&val);

  ASSERT_TRUE(is_eq(T{value}, res));

  g_value_reset(&val);
}

} // anonymous namespace



TEST(Properties, get_undefined)
{
  test_context ctx;

  GError * err = nullptr;
  auto prop = interpeer_wyrd_handle_get_property(ctx.handle,
      ".foo.bar", &err);
  ASSERT_EQ(prop, nullptr);
}



TEST(Properties, set_and_get)
{
  test_context ctx;

  GValue val = G_VALUE_INIT;
  g_value_init(&val, G_TYPE_INT);
  g_value_set_int(&val, 42);

  // Set property
  GError * err = nullptr;
  auto ret = interpeer_wyrd_handle_set_property(ctx.handle,
      ".foo.bar", &val, &err);
  ASSERT_TRUE(ret);
  ASSERT_EQ(err, nullptr);

  // Get property
  auto prop = interpeer_wyrd_handle_get_property(ctx.handle,
      ".foo.bar", &err);
  ASSERT_NE(prop, nullptr);

  auto res = g_value_get_int(prop);
  ASSERT_EQ(res, 42);
  g_value_unset(prop);
  g_free(prop);
}



TEST(Properties, signal)
{
  test_context ctx;

  // Set up to signal
  GError * err = nullptr;
  auto signame = interpeer_wyrd_handle_setup_property_signal(ctx.handle,
      G_OBJECT(ctx.obj), ".foo.bar", &err);
  ASSERT_NE(signal, nullptr);
  ASSERT_EQ(err, nullptr);

  // Connect to signal
  called = 0;
  auto handler_id = g_signal_connect(ctx.obj, signame,
      G_CALLBACK(test_callback), nullptr);
  ASSERT_EQ(called, 0);
  ASSERT_GT(handler_id, 0);

  // Set wyrd property
  GValue val = G_VALUE_INIT;
  g_value_init(&val, G_TYPE_INT);
  g_value_set_int(&val, 42);

  auto ret = interpeer_wyrd_handle_set_property(ctx.handle,
      ".foo.bar", &val, &err);
  ASSERT_TRUE(ret);
  ASSERT_EQ(err, nullptr);

  // At this point, the callback should be called!
  ASSERT_EQ(called, 1);

  g_free(const_cast<char *>(signame));
}



#define CREATE_TEST(type, name, gtype, getter, setter, value)             \
  TEST(Properties, propagate_to_wyrd_ ## type)                            \
  {                                                                       \
    test_propagate_to_wyrd<                                               \
      type, decltype(getter), decltype(setter)                            \
    >(name, value, gtype, getter, setter);                                \
  }                                                                       \
                                                                          \
  TEST(Properties, propagate_to_gobject_ ## type)                         \
  {                                                                       \
    test_propagate_to_gobject<                                            \
      type, decltype(getter), decltype(setter)                            \
    >(name, value, gtype, getter, setter);                                \
  }


CREATE_TEST(int8_t, "i8", G_TYPE_CHAR, g_value_get_schar, g_value_set_schar, 42);
CREATE_TEST(uint8_t, "u8", G_TYPE_UCHAR, g_value_get_uchar, g_value_set_uchar, 42);
CREATE_TEST(int16_t, "i16", G_TYPE_INT, g_value_get_int, g_value_set_int, 42);
CREATE_TEST(uint16_t, "u16", G_TYPE_UINT, g_value_get_uint, g_value_set_uint, 42);
CREATE_TEST(int32_t, "i32", G_TYPE_LONG, g_value_get_long, g_value_set_long, 42);
CREATE_TEST(uint32_t, "u32", G_TYPE_ULONG, g_value_get_ulong, g_value_set_ulong, 42);
CREATE_TEST(int64_t, "i64", G_TYPE_INT64, g_value_get_int64, g_value_set_int64, 42);
CREATE_TEST(uint64_t, "u64", G_TYPE_UINT64, g_value_get_uint64, g_value_set_uint64, 42);

CREATE_TEST(bool, "b", G_TYPE_BOOLEAN, g_value_get_boolean, g_value_set_boolean, TRUE);

CREATE_TEST(float, "f", G_TYPE_FLOAT, g_value_get_float, g_value_set_float, 42.0);
CREATE_TEST(double, "d", G_TYPE_DOUBLE, g_value_get_double, g_value_set_double, 42.0);

TEST(Properties, propagate_to_wyrd_string)
{
  test_propagate_to_wyrd<
    char const *, decltype(g_value_get_string), decltype(g_value_set_string)
  >("string", "hello, world!", G_TYPE_STRING, g_value_get_string, g_value_set_string);
}


TEST(Properties, propagate_to_gobject_string)
{
  test_propagate_to_gobject<
    char const *, decltype(g_value_get_string), decltype(g_value_set_string)
  >("string", "hello, world!", G_TYPE_STRING, g_value_get_string, g_value_set_string);
}
