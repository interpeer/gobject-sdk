# Interpeer SDK for GObject

[![Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)

The Interpeer SDK for GObject makes interpeer technology available to the [GObject](https://docs.gtk.org/gobject/)
ecosystem, e.g. projects built with [Gtk](https://gtk.org/).

The scope of the SDK will change over time - for now, it exposes an easy API
for manipulating [wyrd](/interpeer/wyrd) resources.

**Project Info**

| [ChangeLog](./CHANGES) | [Contributing](./CONTRIBUTING.md) | [Code of Conduct](./CODE_OF_CONDUCT.md) |
|-|-|-|

# 💡 Usage

Being GObject based, the library exposes a C API first and foremost - but a
[GObject Introspection](https://gi.readthedocs.io/en/latest/) file is also
generated, which means bindings to other languages are easy to create. In fact,
our main test driver is the Python binding.

```python
# Load Interpeer SDK binding
import gi
gi.require_version("interpeer", "1.0")
from gi.repository import interpeer

# Create API instance
api = interpeer.WyrdApi()

# Create resource handle
flags = interpeer.WyrdOpenFlags.OPEN_RW | interpeer.WyrdOpenFlags.OPEN_CREATE
handle = api.open_resource("testresource", flags)

# Manipulate Wyrd property
ret = handle.set_property(".foo.bar", 123)
```
# 📖 API

The API is growing as the needs of the [Interpeer Project](https://interpeer.io)
change. The [full documentation](https://docs.interpeer.io/gobject-sdk/) provides
How-Tos and an API reference.

# 🛠️ Installation

If you're using meson, just put this repo (at a version tag of your choice) into
your subprojects as a [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules),
e.g.

```bash
$ git submodule add https://codeberg.org/interpeer/gobject-sdk subprojects/gobject-sdk
$ cd subprojects/gobject-sdk
$ git checkout v0.1.0 # or whatever
$ cd ..
$ git commit -m "Added gobject-sdk at v0.1.0"
```

When that is done, you can just use the SDK in your own `meson.build` file.

```python
# Try a system installed SDK first, fall back to subproject
interpeer_dep = dependency(
  'interpeer_gobject_shared',
  fallback: ['gobject-sdk']
)

# Assuming you're building a list of dependencies
deps += [interpeer_dep]

summary('Interpeer', interpeer_dep.version(), section: 'Interpeer Dependencies')
```

# ⚖️ License

This SDK is licensed under the [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html);
[a copy of the license](./LICENSE) is in the repository.

For other licensing options, please contact [Interpeer gUG](https://interpeer.io).

We're a non-profit, however, so if you like this library, please consider
[donating ❤️](https://interpeer.io/donations/). That will make sure the code
stays maintained.
