/*
 * This file is part of gobject-sdk.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTERPEER_GOBJECT_SDK_WYRD_API_H
#define INTERPEER_GOBJECT_SDK_WYRD_API_H

#include <glib-object.h>

#include <wyrd/api.h>

#include <interpeer/gobject/visibility.h>
#include <interpeer/gobject/wyrd-handle.h>

G_BEGIN_DECLS

/**
 * interpeer_wyrd_error_quark:
 *
 * Quark for WYRD_ERR_* errors.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
GQuark
interpeer_wyrd_error_quark(void);


/**
 * interpeer_vessel_error_quark:
 *
 * Quark for VESSEL_ERR_* errors.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
GQuark
interpeer_vessel_error_quark(void);



/**
 * WyrdApi:
 *
 * Create a Wyrd API instance. This is the entry point for all wyrd functionality.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
G_DECLARE_FINAL_TYPE(WyrdApi, interpeer_wyrd_api, INTERPEER, WYRD_API, GObject)

#define INTERPEER_TYPE_WYRD_API (interpeer_wyrd_api_get_type())

/**
 * interpeer_wyrd_api_open_file:
 * @filename: The (UTF-8 encoded) name of the file to open.
 * @flags: Flags specifying how to open the file (see #WyrdOpenFlags)
 *
 * Opens an otherwise unstructured file for storing/retrieving wyrd properties.
 *
 * ::: note
 *     This function should not be used, and is marked deprecated as a result.
 *     It exists mostly for testing purposes. Use
 *     [method@WyrdApi.open_resource] instead.
 *
 * Return: (transfer full)
 *
 * Since: 0.1.0
 * Stability: Stable
 * Deprecated: 0.1.0: Use [method@WyrdApi.open_resource] instead.
 */
INTERPEER_GOBJECT_SDK_API
WyrdHandle *
interpeer_wyrd_api_open_file(WyrdApi * self, char const * filename,
    WyrdOpenFlags flags, GError ** error);

/**
 * interpeer_wyrd_api_open_resource:
 * @identifier: The (UTF-8 encoded) identifier of the resource to open.
 * @flags: Flags specifying how to open the resource (see #WyrdOpenFlags)
 *
 * Opens a [vessel](https://interpeer.io/projects/vessel/) resource with the
 * given identifier for storing wyrd properties within.
 *
 * Return: (transfer full)
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
WyrdHandle *
interpeer_wyrd_api_open_resource(WyrdApi * self, char const * identifier,
    WyrdOpenFlags flags, GError ** error);

G_END_DECLS

#endif // guard
