/*
 * This file is part of gobject-sdk.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTERPEER_GOBJECT_SDK_WYRD_HANDLE_H
#define INTERPEER_GOBJECT_SDK_WYRD_HANDLE_H

#include <glib-object.h>

#include <wyrd/handle.h>

#include <interpeer/gobject/visibility.h>

G_BEGIN_DECLS

/**
 * WyrdOpenFlags:
 * @W_O_NONE: No open mode; this is largely useless.
 * @W_O_READ: Open in read mode *only*.
 * @W_O_WRITE: Open in write mode *only*.
 * @W_O_RW: Open in read and write mode.
 * @W_O_CREATE: Create resource file if it doesn't exist yet.
 * @W_O_SKIP_UNKNOWN: When encountering unknown parts of the file, try and skip
 *    them.
 *
 * Open flags for a [class@WyrdHandle]; note that the values are defined in the
 * wyrd library, and just exposed here as a #GFlags type.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
typedef enum
{
  W_O_NONE          = WYRD_O_NONE,
  W_O_READ          = WYRD_O_READ,
  W_O_WRITE         = WYRD_O_WRITE,
  W_O_RW            = WYRD_O_RW,
  W_O_CREATE        = WYRD_O_CREATE,
  W_O_SKIP_UNKNOWN  = WYRD_O_SKIP_UNKNOWN,
} WyrdOpenFlags;

#define INTERPEER_TYPE_WYRD_OPEN_FLAGS interpeer_wyrd_open_flags_get_type()
INTERPEER_GOBJECT_SDK_API
GType interpeer_wyrd_open_flags_get_type(void) G_GNUC_CONST;

/**
 * WyrdHandle:
 *
 * A handle representing either a vessel resource or a file containing
 * wyrd properties.
 *
 * Opened via [class@WyrdApi]'s member functions.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
G_DECLARE_FINAL_TYPE(WyrdHandle, interpeer_wyrd_handle, INTERPEER, WYRD_HANDLE, GObject)

#define INTERPEER_TYPE_WYRD_HANDLE (interpeer_wyrd_handle_get_type())

/**
 * interpeer_wyrd_handle_set_author:
 * @author: A PEM encoded authoring (private) key.
 *
 * On a vessel handle, set the authoring key to use when creating new extents.
 *
 * Return: sucess or failure.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
gboolean
interpeer_wyrd_handle_set_author(WyrdHandle * self, gchar const * author,
    GError ** error);

/**
 * interpeer_wyrd_handle_set_algorithm_choices:
 * @choices: Algorithm choices for creating extents.
 *
 * On a vessel handle, set the algorithms to use when creating extents. If this
 * function is not called, default algorithm choices will be used instead.
 *
 * Return: sucess or failure.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
gboolean
interpeer_wyrd_handle_set_algorithm_choices(WyrdHandle * self, gchar const * choices,
    GError ** error);


/**
 * interpeer_wyrd_handle_set_property:
 * @path: The path of the wyrd property to manipulate.
 * @value: The new value to set the property to.
 *
 * Sets the property at @path to @value.
 *
 * Return: success or failure.
 *
 * Stability: Unstable
 * Since: 0.1.0
 */
INTERPEER_GOBJECT_SDK_API
gboolean
interpeer_wyrd_handle_set_property(WyrdHandle * self, gchar const * path,
    GValue * value, GError ** error);


/**
 * interpeer_wyrd_handle_get_property:
 * @path: The path of the wyrd property to retrieve.
 *
 * Retrieves the value of the property at @path.
 *
 * Return: (transfer full): The current property value or NULL.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
GValue *
interpeer_wyrd_handle_get_property(WyrdHandle * self, gchar const * path,
    GError ** error);


/**
 * interpeer_wyrd_handle_property_signal_connect:
 * @target: The target object for the signal.
 * @target_property: The name of the GObject property of @target that should
 *    be connected to the wyrd property.
 * @wyrd_property_path: The path of the wyrd property to connect.
 *
 * Connect the @target_property of @target to a `notify::` signal handler that
 * forwards value changes to the property at @wyrd_property_path. Also, install
 * a wyrd callback for this wyrd property that forwards updated values to
 * @target_property.
 *
 * When the #WyrdHandle instance is destroyed, all signals connected via this
 * function are destroyed.
 *
 * Return: the signal handler ID.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
gulong
interpeer_wyrd_handle_property_connect(WyrdHandle * self,
    GObject * target, gchar const * target_property, gchar const * wyrd_property_path,
    GError ** error);

/**
 * interpeer_wyrd_handle_setup_property_signal:
 * @target: The target object for the signal.
 * @property_path: The path of the wyrd property to observe.
 *
 * Sets up a signal for the @target class to receive that will be emitted when
 * the wyrd property at @property_path changes. The signal will have a detail,
 * which is the @property_path, meaning you can set up multiple signals for
 * multiple properties.
 *
 * The emitted signal will have the following arguments:
 * - The #WyrdHandle of the document.
 * - The name of the updated wyrd property.
 * - The new value of that wyrd property, as a GValue type.
 *
 * Note that it is safe to set this or other properties from within the signal
 * callback. If you set the same property, the signal will not be emitted again,
 * preventing infinite loops.
 *
 * When the #WyrdHandle instance is destroyed, all signals connected via this
 * function are destroyed.
 *
 * Note that the @target instance is *not* connected to this signal. This is
 * because bindings to other languages than C make this difficult to achieve.
 * This is why the function returns the full signal name, so that you can
 * connect the target afterwards.
 *
 * Return: (transfer full): The full name of the signal (with detail)
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
gchar const *
interpeer_wyrd_handle_setup_property_signal(WyrdHandle * self,
    GObject * target, gchar const * property_path,
    GError ** error);


G_END_DECLS

#endif // guard
