/*
 * This file is part of gobject-sdk.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTERPEER_GOBJECT_SDK_VERSION_H
#define INTERPEER_GOBJECT_SDK_VERSION_H

#include <glib-object.h>

#include <stdint.h>

#include <interpeer/gobject/visibility.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * VersionData:
 * @major: The SDK major version.
 * @minor: The SDK minor version.
 * @patch: The SDK patch version.
 *
 * The SDK's major, minor and patch version numbers are provided in this
 * struct.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
typedef struct
{
  uint16_t major;
  uint16_t minor;
  uint16_t patch;
} VersionData;

#define INTERPEER_TYPE_VERSION_DATA (interpeer_version_data_get_type())

INTERPEER_GOBJECT_SDK_API
GType
interpeer_version_data_get_type(void);

/**
 * interpeer_version_data_copy:
 *
 * Copy a version number (boxed type).
 *
 * Return: (transfer full)
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
VersionData *
interpeer_version_data_copy(VersionData * v);


/**
 * interpeer_version_data_free:
 *
 * Free a version number (boxed type).
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
void
interpeer_version_data_free(VersionData * v);


/**
 * interpeer_version_data_new: (constructor)
 *
 * Return the SDK's version number.
 *
 * Return: (transfer full): The SDK version struct.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
VersionData * interpeer_version_data_new(void);


/**
 * interpeer_copyright_string:
 *
 * Return a copyright string for this SDK; the returned value must not be
 * freed.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
char const * interpeer_copyright_string(void);


/**
 * interpeer_license_string:
 *
 * Return a license string for this SDK; the returned value must not be
 * freed.
 *
 * Since: 0.1.0
 * Stability: Stable
 */
INTERPEER_GOBJECT_SDK_API
char const * interpeer_license_string(void);


#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus


#endif // guard
