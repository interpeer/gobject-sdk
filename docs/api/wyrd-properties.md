Title: Wyrd Properties
SPDX-License-Identifier: GPLv3
SPDX-FileCopyrightText: 2023 Interpeer gUG (haftungsbeschraenkt)

# Initialize the Library

The SDK needs to manage some internal state, which means the first thing that
a user of the library needs to do is to initialize a library handle by
instanciating [class@WyrdApi].

Note that we use Python as the example language here, as it is fairly concise.
The GObject bindings mean you can use your preferred supported language,
however.

```python
import gi

gi.require_version("interpeer", "1.0")
from gi.repository import interpeer

api = interpeer.WyrdApi()
```

::: note
    The class name is `WyrdApi` in reference to the [wyrd](https://interpeer.io/projects/wyrd/)
    library which it wraps. This could evolve in future.

With an api handle in hand, it is possible to open Wyrd documents. Wyrd
documents provide access to structured data via properties (though not the
GObject kind). Properties have a name, a type and a value.

```python
flags = interpeer.WyrdOpenFlags.OPEN_RW | interpeer.WyrdOpenFlags.OPEN_CREATE
handle = api.open_resource("testresource", flags)
```

## Vessel Configuration

Wyrd documents are actually [vessel](https://interpeer.io/projects/vessel)
resources. That means you need to also specify which algorithms to use for
this container format. Below is a good default if you don't want to know
more for now.

```python
handle.set_algorithm_choices('sha3-512;sha3-512;none;sha3-512;eddsa;ed25519;kmac128;chacha20;aead;ph=1')
```

Additionally, you need to set an authoring key. This is a private key in PEM
format that you need to keep with your application state. Updates will be signed
with this key, so that other parties can verify you're the author.

```python
handle.set_author('-----BEGIN PRIVATE KEY----- ... -----END PRIVATE KEY-----')
```

# Property Usage

With the handle open, you can query and set properties. Of course a newly
created resource will not contain any properties, so the example below will
not yield results initially. But the principle holds.

Properties have hierarchical names, using the `.` (dot, full-stop) as a
separator. The root element in the hierarchy has no name, which means that a
property named `.foo` is equivalent to one named `foo`.

```python
val = handle.get_property('.foo.bar')
# val is None if the resource is new/the property does not exist

ret = handle.set_property('.foo.bar', 42)
assert ret

val = handle.get_property('.foo.bar')
assert val == 42
```

Properties can have any primitive #GValue type such as integers or
floats, or strings.

::: seealso
    [Getting notified of property changes](property-changes.html)
