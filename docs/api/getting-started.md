Title: Getting Started
SPDX-License-Identifier: GPLv3
SPDX-FileCopyrightText: 2023 Interpeer gUG (haftungsbeschraenkt)

# Getting Started

The GObject SDK is largely implemented in portable C, but offers a C-only API,
because GObject is a C library.

You would usually use meson when contributing to the
library itself; that build system also provides some means for dependency
management, which may help you in your project. If you do not wish to use
Meson for whichever reason, try the approach with :ref:`Conan <using-conan>`
below.

# Using Meson

GObject SDK is built using [meson](https://mesonbuild.com), because this
provides cross-platform builds that are relatively easily managed. Meson
is written in [Python](https://python.org), as is gi-docgen, the tool with
which this documentation is generated.

## Install Tooling

Because there is so much Python based tooling in this project, it seemed
best to use Pythonic means for managing the tool dependencies, so that is
what we're doing.

1. Install Python version 3 or above. Most Linux systems have this already
    installed; BSD systems provide it. Use your system's best installation
    method for this.

    Python comes with a command `pip` (sometimes `pip3`), with which you
    install packages.

1. Install the `pipenv` package:

    ```
    $ pip3 install -U pipenv
    ```

1. Use pipenv to install the development tools:

    ```
    $ pipenv install
    ```

1. Activate the pipenv virtual environment:

    ```
    $ pipenv shell
    ```

At this point the shell you're running has the development tools installed
locally.

## Build with Meson

Meson expects a `build` folder separate from the source folder. Typically
you might create this within the repository working directory; this is what
we're doing here in the example.

```shell
$ cd /path/to/repo
$ mkdir build
$ cd build
```

From there, you need to run Meson.

```shell
$ meson setup ..
$ ninja
```

For more information on Meson usage, see
[the meson documentation](https://mesonbuild.com). You may need to install
[ninja](https://ninja-build.org/) separately.

## Documentation

For generating documentation, we need `gi-docgen`.
This is installed by passing an extra flag to the pipenv
installation step above:

```shell
$ pipenv install --dev
```

To build the documentation, do the following:

```shell
$ cd /path/to/repo/docs/api
$ make html
```

## Using Conan

We provide (semi-)regular builds of the library using
[conan](https://conan.io/). These are hosted on codeberg, where our code is
also hosted. The site also [includes instructions on installing
the SDK via conan ](https://codeberg.org/interpeer/-/packages/conan/gobject-sdk/).
