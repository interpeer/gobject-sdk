option('build_extras', type: 'boolean',
  value: true,
  description: '''Build extras such as tests, examples, etc.'''
)
